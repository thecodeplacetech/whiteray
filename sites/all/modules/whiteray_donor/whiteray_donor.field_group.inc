<?php
/**
 * @file
 * whiteray_donor.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function whiteray_donor_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_donordetails|node|donor_details|form';
  $field_group->group_name = 'group_donordetails';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'donor_details';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Donor Details',
    'weight' => '0',
    'children' => array(
      0 => 'field_donor_id',
      1 => 'field_full_name',
      2 => 'field_postal_address',
      3 => 'field_gender',
      4 => 'field_phone_landline_',
      5 => 'field_phone_mobile_',
      6 => 'field_dob',
      7 => 'field_date_first_donation',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_donordetails|node|donor_details|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_familydetails|node|donor_details|form';
  $field_group->group_name = 'group_familydetails';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'donor_details';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Family Details',
    'weight' => '1',
    'children' => array(
      0 => 'field_family',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_familydetails|node|donor_details|form'] = $field_group;

  return $export;
}
