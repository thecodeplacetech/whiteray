<?php
/**
 * @file
 * whiteray_donor.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function whiteray_donor_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function whiteray_donor_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function whiteray_donor_node_info() {
  $items = array(
    'donor_details' => array(
      'name' => t('Donor Details'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
