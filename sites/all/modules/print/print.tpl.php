<?php
include('/whiteray/sites/all/modules/custommodule/custom_button/index.php');

?>
<?php
/**
 * @file
 * Default theme implementation to display a printer-friendly version page.
 *
 * This file is akin to Drupal's page.tpl.php template. The contents being
 * displayed are all included in the $content variable, while the rest of the
 * template focuses on positioning and theming the other page elements.
 *
 * All the variables available in the page.tpl.php template should also be
 * available in this template. In addition to those, the following variables
 * defined by the print module(s) are available:
 *
 * Arguments to the theme call:
 * - $node: The node object. For node content, this is a normal node object.
 *   For system-generated pages, this contains usually only the title, path
 *   and content elements.
 * - $format: The output format being used ('html' for the Web version, 'mail'
 *   for the send by email, 'pdf' for the PDF version, etc.).
 * - $expand_css: TRUE if the CSS used in the file should be provided as text
 *   instead of a list of @include directives.
 * - $message: The message included in the send by email version with the
 *   text provided by the sender of the email.
 *
 * Variables created in the preprocess stage:
 * - $print_logo: the image tag with the configured logo image.
 * - $content: the rendered HTML of the node content.
 * - $scripts: the HTML used to include the JavaScript files in the page head.
 * - $footer_scripts: the HTML  to include the JavaScript files in the page
 *   footer.
 * - $sourceurl_enabled: TRUE if the source URL infromation should be
 *   displayed.
 * - $url: absolute URL of the original source page.
 * - $source_url: absolute URL of the original source page, either as an alias
 *   or as a system path, as configured by the user.
 * - $cid: comment ID of the node being displayed.
 * - $print_title: the title of the page.
 * - $head: HTML contents of the head tag, provided by drupal_get_html_head().
 * - $robots_meta: meta tag with the configured robots directives.
 * - $css: the syle tags contaning the list of include directives or the full
 *   text of the files for inline CSS use.
 * - $sendtoprinter: depending on configuration, this is the script tag
 *   including the JavaScript to send the page to the printer and to close the
 *   window afterwards.
 *
 * print[--format][--node--content-type[--nodeid]].tpl.php
 *
 * The following suggestions can be used:
 * 1. print--format--node--content-type--nodeid.tpl.php
 * 2. print--format--node--content-type.tpl.php
 * 3. print--format.tpl.php
 * 4. print--node--content-type--nodeid.tpl.php
 * 5. print--node--content-type.tpl.php
 * 6. print.tpl.php
 *
 * Where format is the ouput format being used, content-type is the node's
 * content type and nodeid is the node's identifier (nid).
 *
 * @see print_preprocess_print()
 * @see theme_print_published
 * @see theme_print_breadcrumb
 * @see theme_print_footer
 * @see theme_print_sourceurl
 * @see theme_print_url_list
 * @see page.tpl.php
 * @ingroup print
 */


?>
<?php

  $currentprint = arg(2);
    switch($currentprint){

    case "label":
      ?>
          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
          "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
          <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>">
          <head>
  
            <?php print $scripts; ?>
            <?php if (isset($sendtoprinter)) print $sendtoprinter; ?>
            <?php print $robots_meta; ?>
            <?php if (theme_get_setting('toggle_favicon')): ?>
              <link rel='shortcut icon' href='<?php print theme_get_setting('favicon') ?>' type='image/x-icon' />
            <?php endif; ?>
            <?php print $css; ?>
          </head>
          <body>

            <!--Print Label Field -->
            
            <?php
            $display = array('label');
            $view = field_view_field('node', $node, 'field_full_name', $display);
            print drupal_render($view);
            ?>  

            <?php
            $display = array('label');
            $view = field_view_field('node', $node, 'field_address', $display);
            print drupal_render($view);
            ?>


            <!-- End Of Print Label Field-->
 
          </body>
          </html>
      <?php
    break;

    case "receipt":
      ?>
          <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
           "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
          <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>">
            <head>
          <?php print $scripts; ?>
          <?php if (isset($sendtoprinter)) print $sendtoprinter; ?>
          <?php print $robots_meta; ?>
          <?php if (theme_get_setting('toggle_favicon')): ?>
            <link rel='shortcut icon' href='<?php print theme_get_setting('favicon') ?>' type='image/x-icon' />
          <?php endif; ?>
          <?php print $css; ?>
        </head>
        <body>

          <!--Here print the receipt field Value-->

          <img src="/whiteray/sites/all/modules/custommodule/custom_button/logo.jpg" height="250px" width="800px" >
 <div margin-top:50px>

           <?php echo ("Office:: #31/50, Sai Park Periyalwar Street,Sundaram Colony,East Tambaram,Chennai-600 059,India.
          Home::Eraiyur Village(Near Vallakottai Murugan Temple)Mathur Post,Sriperumbudar Taluk,Kanchipuram District-602 105,TN
          Office:: Tel No. :+91 44 2239 9551, +91 44 43574464,+91 44-22392424 | Home :: Tel. No. :+91 44 27192146 \n</br> \n</br>");  ?>
 </div >
          <div align=center>
          <strong>
            <?php echo ("RECEIPT");?>
          </strong>
          </div>

          <div align=right>
          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_receiptno', $display);
          print drupal_render($view);
          ?>
          </div>

          <div align=right>
          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_id_donor', $display);
          print drupal_render($view);
          echo "Receipt date :".date('d/m/Y');
          ?>
          </div>

          <div align=right>
          <?php 
          $display = array('label');
          $view = field_view_field('node', $node, 'field_donor', $display);
          print drupal_render($view);
          ?>
          </div>

          <?php echo "Received with thanks From"; ?>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_full_name', $display);
          print drupal_render($view);
          ?>

          <?php $name = $node->field_full_name['und'][0]['value'];
            print_r($name);

          ?>

          <?php echo "Donation \n<br />"; ?>

          <?php echo "\n<br />"?>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_amount', $display);
          print drupal_render($view);
          ?>

          <?php $link = $node->field_amount['und'][0]['value'];
            
          //print number to words convert here

          
       
      

    function convert_number_to_words($number) {
  
    $textfield = "";
    $hyphen      = '-';
    $conjunction = '  ';
    $separator   = ' ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'Zero',
        1                   => 'One',
        2                   => 'Two',
        3                   => 'Three',
        4                   => 'Four',
        5                   => 'Five',
        6                   => 'Six',
        7                   => 'Seven',
        8                   => 'Eight',
        9                   => 'Nine',
        10                  => 'Ten',
        11                  => 'Eleven',
        12                  => 'Twelve',
        13                  => 'Thirteen',
        14                  => 'Fourteen',
        15                  => 'Fifteen',
        16                  => 'Sixteen',
        17                  => 'Seventeen',
        18                  => 'Eighteen',
        19                  => 'Nineteen',
        20                  => 'Twenty',
        30                  => 'Thirty',
        40                  => 'Fourty',
        50                  => 'Fifty',
        60                  => 'Sixty',
        70                  => 'Seventy',
        80                  => 'Eighty',
        90                  => 'Ninety',
        100                 => 'Hundred',
        1000                => 'Thousand',
        1000000             => 'Million',
        1000000000          => 'Billion',
        1000000000000       => 'Trillion',
        1000000000000000    => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );
   
    if (!is_numeric($number)) {
        return false;
    }
   
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
   
    $string = $fraction = null;
   
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
   
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
   
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
   
      return $string;
    }
    $amount = $link;
      convert_number_to_words($amount);

      echo '<b>'.convert_number_to_words($amount).'</b>';

          //print number to words converter

           ?>



          <?php echo "\n<br />"?>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_payment_mode', $display);
          print drupal_render($view);
          ?>

          <?php echo "\n<br />"?>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_dd_neft_cheque_no', $display);
          print drupal_render($view);
          ?>

          <?php echo "\n<br />"?>

          <?php echo "Note:PAN NO. AAAANO136F" ?>

          

          <div align=left>
          <?php echo ("Donation are exempted from Income Tax Act 1961 \n<br /> Registration u/s 12(A) Order No. 2039/205/96-97/ \n<br />TN-V Dt.25.7.1997 and Under Section 80(G)  of IT \n<br /> Act vide Order No.DIT (E) No. 1(106) 01-02 Dated \n<br /> 30-05-2008 \n<br /> \n<br />");?>
          </div>

          <div align=left>
          <?php echo ("(Cheque/Draft are subjected to realisation)");?>
        </div>

        <?php echo "\n<br />"?>

        <div align=right>
            <?php echo ("For The New Life Charitable Trust \n<br />");?>
          <div>

           <?php echo "\n<br />"?>

          <div align=right>
          <?php echo ("(Mrs. G. Lalitha) \n<br /> Founder Trustee \n<br />");?>
          
          </div>

          <!--End of Print Receipt-->

        </body>
        </html>
    
      <?php
    break;

    case "thanks_giving_letter":
      ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
           "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
          <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>">
            <head>

          <?php print $scripts; ?>
          <?php if (isset($sendtoprinter)) print $sendtoprinter; ?>
          <?php print $robots_meta; ?>
          <?php if (theme_get_setting('toggle_favicon')): ?>
            <link rel='shortcut icon' href='<?php print theme_get_setting('favicon') ?>' type='image/x-icon' />
          <?php endif; ?>
          <?php print $css; ?>
        </head>
        <body>
        
          
              <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this tag, the sky will fall on your head -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>The Code Place Newsletter Template</title>
  
<style>
/* ------------------------------------- 
    GLOBAL 
------------------------------------- */
* { 
  margin:0;
  padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

img { 
  max-width: 100%; 
}
.collapse {
  margin:0;
  padding:0;
}
body {
  -webkit-font-smoothing:antialiased; 
  -webkit-text-size-adjust:none; 
  width: 100%!important; 
  height: 100%;
}


/* ------------------------------------- 
    ELEMENTS 
------------------------------------- */
a { color: #2BA6CB;}

.btn {
  text-decoration:none;
  color: #FFF;
  background-color: #666;
  padding:10px 16px;
  font-weight:bold;
  margin-right:10px;
  text-align:center;
  cursor:pointer;
  display: inline-block;
}

p.callout {
  padding:15px;
  background-color:#FE722D;
  margin-bottom: 15px;
}
.callout a {
  font-weight:bold;
  color: #2BA6CB;
}

table.social {
/*  padding:15px; */
  background-color: #ebebeb;
  
}
.social .soc-btn {
  padding: 3px 7px;
  font-size:12px;
  margin-bottom:10px;
  text-decoration:none;
  color: #FFF;font-weight:bold;
  display:block;
  text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.tw { background-color: #1daced!important; }
a.gp { background-color: #DB4A39!important; }
a.ms { background-color: #000!important; }

.sidebar .soc-btn { 
  display:block;
  width:100%;
}

/* ------------------------------------- 
    HEADER 
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


/* ------------------------------------- 
    BODY 
------------------------------------- */
table.body-wrap { width: 100%;}


/* ------------------------------------- 
    FOOTER 
------------------------------------- */
table.footer-wrap { width: 100%;  clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
  font-size:10px;
  font-weight: bold;
  
}


/* ------------------------------------- 
    TYPOGRAPHY 
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:900; font-size: 17px;}
h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}

.collapse { margin:0!important;}

p, ul { 
  margin-bottom: 10px; 
  font-weight: normal; 
  font-size:14px; 
  line-height:1.6;
}
p.lead { font-size:17px; }
p.last { margin-bottom:0px;}

ul li {
  margin-left:5px;
  list-style-position: inside;
}

/* ------------------------------------- 
    SIDEBAR 
------------------------------------- */
ul.sidebar {
  background:#ebebeb;
  display:block;
  list-style-type: none;
}
ul.sidebar li { display: block; margin:0;}
ul.sidebar li a {
  text-decoration:none;
  color: #666;
  padding:10px 16px;
/*  font-weight:bold; */
  margin-right:10px;
/*  text-align:center; */
  cursor:pointer;
  border-bottom: 1px solid #777777;
  border-top: 1px solid #FFFFFF;
  display:block;
  margin:0;
}
ul.sidebar li a.last { border-bottom-width:0px;}
ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



/* --------------------------------------------------- 
    RESPONSIVENESS
    Nuke it from orbit. It's the only way to be sure. 
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
  display:block!important;
  max-width:600px!important;
  margin:0 auto!important; /* makes it centered */
  clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
  padding:15px;
  max-width:600px;
  margin:0 auto;
  display:block; 
}

/* Let's make sure tables in the content area are 100% wide */
.content table { width: 100%; }


/* Odds and ends */
.column {
  width: 300px;
  float:left;
}
.column tr td { padding: 15px; }
.column-wrap { 
  padding:0!important; 
  margin:0 auto; 
  max-width:600px!important;
}
.column table { width:100%;}
.social .column {
  width: 280px;
  min-width: 279px;
  float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }


/* ------------------------------------------- 
    PHONE
    For clients that support media queries.
    Nothing fancy. 
-------------------------------------------- */
@media only screen and (max-width: 600px) {
  
  a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

  div[class="column"] { width: auto!important; float:none!important;}
  
  table.social div[class="column"] {
    width:auto!important;
  }

}



/* We are using CSS3 Property "transition" for the animating blinking text effect */
P {transition: color 200ms ease;}
 
/* The class "Blink" is set to be transparent*/
P.blink {color:transparent;}



</style>



</head>

<!-- HEADER -->
<table class="head-wrap">
  <tr style="background: #fff url('pattern0.png') repeat;">
    <td></td>
    <td class="header container">
      
        <div class="content">
          <table>
          <tr>
            <td><img src="/whiteray/sites/all/modules/custommodule/custom_button/logo.jpg" height="200px" width="800px" ></td>
          </tr>
        </table>
        </div>
        
    </td>
    <td></td>
  </tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap">
  <tr>
    <td></td>
    <td class="container" bgcolor="#FFFFFF">

      <div class="content">
      <table>
        <tr>
          <td>
            
            <p align="left">REF:NLCT/VL/RPT-818/15-16</p>
            <p align="right"><?php echo date('d/m/Y'); ?></p>
            <p>To</br>
               Address1</p></br>
               Address2</p>
          
            
            <!-- A Real Hero (and a real human being) -->
            
            
            <!-- Callout Panel 
            <p class="callout" >
              Process facilitation is a critical component of increasing effectiveness in the workings of business, government and communities across the globe to help <b>groups</b> build consensus, maximize participation and move towards action within organizations. It creates and enhances engagement in groups and builds ownership of actions among groups. <a style="text-decoration:none; color: white" href="http://www.iaf-india.org/unplugged-2015/registration-2/"target="_blank"> Register for Conference </a>
            </p><!-- /Callout Panel </p>-->
            
            
            
            WARM GREETINGS!!
            <p align="center">"Love is that condition in which the happiness of another Person is essential to your own joy"- Bulwer Lytton</p>

            <strong>Dear Sir/Madam,</strong>
            <p>New Life immensely thank your good self for having donated of -----------------------------------towards in on -/-/2015. Please find the Receipt No:----------/-/2015 for the donation made to New </p>

            <p>Life.We are confident that the prayers of the Residents will always be with you and your family as a shield and bring peace and prosperity throughout your lives. </p>
            
            <p>As you are aware that the trust is slowly building the CORPUS FUND such help is very supportive. While thanking you once again we take this opportunity to invite you to visit New Life as and when possible and     thus feel the pleasure of joy in giving.</p>
            
            <p>We pray the Almighty to shower His blessings on you for the Concern and Compassion you have for the poor.     We expect your continuous generosity and look you to make this New Life an outstanding example of selfless service. Kindly visit our website www.newlifecharitabletrust.org</p>
            
            <p>With Regards,</p>
            
            
            
            <p>Mrs.G.Lalitha</br>
               Founder Trustee</br>
               For The New Life Charitable Trust</p>






</body>
</html>

      <?php
    break;
    case "office_copy":
    ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
           "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
          <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>">
            <head>
          <?php print $scripts; ?>
          <?php if (isset($sendtoprinter)) print $sendtoprinter; ?>
          <?php print $robots_meta; ?>
          <?php if (theme_get_setting('toggle_favicon')): ?>
            <link rel='shortcut icon' href='<?php print theme_get_setting('favicon') ?>' type='image/x-icon' />
          <?php endif; ?>
          <?php print $css; ?>
        </head>
        <body>
         
          <!--Print Office Copy -->
          <img src="/whiteray/sites/all/modules/custommodule/custom_button/logo.jpg" height="250px" width="800px" >


          <?php echo ("Office:: #31/50, Sai Park Periyalwar Street,Sundaram Colony,East Tambaram,Chennai-600 059,India.
          Home::Eraiyur Village(Near Vallakottai Murugan Temple)Mathur Post,Sriperumbudar Taluk,Kanchipuram District-602 105,TN
          Office:: Tel No. :+91 44 2239 9551, +91 44 43574464,+91 44-22392424 | Home :: Tel. No. :+91 44 27192146 \n</br> \n</br>");  ?>

          <div align=center>
            <strong>
              <?php echo (" RECEIPT ");?>
            </strong>
          </div>

          <div align=center>
          <?php echo ("(Office copy)");?>
          </div>

          <div align=right>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_receiptno', $display);
          print drupal_render($view);
          ?>
          </div>

          <div align=right>
          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_id_donor', $display);
          print drupal_render($view);
          echo "Receipt date :".date('d/m/Y');
          ?>
          </div>

          <div align=right>
          <?php 
          $display = array('label');
          $view = field_view_field('node', $node, 'field_donor', $display);
          print drupal_render($view);
          ?>
          </div>


          <?php echo "Received with Thanks From"; ?>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_full_name', $display);
          print drupal_render($view);
          ?>

          <?php echo "Donation \n<br />"; ?>
          <?php echo "\n<br />"?>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_amount', $display);
          print drupal_render($view);
          ?>
          <?php echo "\n<br />"?>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_payment_mode', $display);
          print drupal_render($view);
          ?>

          <?php echo "\n<br />"?>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_dd_neft_cheque_no', $display);
          print drupal_render($view);
          ?>

          <?php echo "\n<br />"?>

          <?php echo "Note:PAN NO. AAAANO136F" ?>

          

          <div align=left>
          <?php echo ("Donation are exempted from Income Tax Act 1961 \n<br /> Registration u/s 12(A) Order No. 2039/205/96-97/ \n<br />TN-V Dt.25.7.1997 and Under Section 80(G)  of IT \n<br /> Act vide Order No.Dit (E) No. 1(106) 01-02 Dated \n<br /> 30-05-2008 \n<br /> \n<br />");?>
          </div>

           <div align=left>
          <?php echo ("(Cheque/Draft are subjected to realisation)");?>
          </div>

          <?php echo "\n<br />"?>


          <div align=left>
          <?php echo ("Sponsor For:Oil Purpose");?>
          </div>

          <?php echo "\n<br />"?>

          <div align=left>
          <?php echo ("Purpose:Donation");?>
          </div>

          <?php echo "\n<br />"?>

          <?php
          $display = array('label');
          $view = field_view_field('node', $node, 'field_occasion', $display);
          print drupal_render($view);
          ?>

          <div align=right>
            <?php echo ("For The New Life Charitable Trust \n<br />");?>
          <div>

          <?php echo "\n<br />"?>

          <div align=right>
          <?php echo ("(Mrs. G. Lalitha) \n<br /> Founder Trustee \n<br />");?>
          <?php echo "Sponsor date :".date('d/m/Y'); ?>
          </div>
          <!--End of print office copy-->


        </body>
        </html>
    <?php
    break;

    }
?>